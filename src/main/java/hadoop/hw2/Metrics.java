package hadoop.hw2;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.min;
import static org.apache.spark.sql.functions.sum;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

class Aggregator{
	/**
	 * Aggregates the data in the given csv file by the given type of aggregation
	 * @param fileName 
	 * @param aggFunc determines the type of aggregation -sum, -avg or -min
	 */
	public static Dataset<Row> process(Dataset<Row> df, String aggFunc){
		
        Dataset<Row> aggregated;
        if(aggFunc.equals("-avg"))
        	aggregated = df.groupBy("metricid").agg(avg("value").as("agg_value")).withColumn("type", lit("avg"));
        else if(aggFunc.equals("-sum"))
        	aggregated = df.groupBy("metricid").agg(sum("value").as("agg_value")).withColumn("type", lit("sum"));
        else
        	aggregated = df.groupBy("metricid").agg(min("value").as("agg_value")).withColumn("type", lit("min"));
        aggregated.show();
        df = aggregated.join(df.groupBy("metricid").agg(min("timestamp").as("timestamp")),"metricid");
        df = df.select("metricid","timestamp","type","agg_value");
        return df;
	}
}
public class Metrics 
{
    public static void main( String[] args )
    {
    	if(args.length != 3){
    		System.out.println("Usage: metrics <inputFiles> <outputDir> <aggregateFunc>");
    		return;
    	}
    	String aggFunc = args[2];
        SparkSession spark = SparkSession.builder().appName("Metrics").getOrCreate();
        Dataset<Row> df = spark.read().csv(args[0])
        		.withColumnRenamed("_c0", "metricid")
        		.withColumnRenamed("_c1", "timestamp")
        		.withColumnRenamed("_c2", "value");
        Aggregator.process(df, aggFunc).write()
        		  .mode(SaveMode.Overwrite)
        		  .csv(args[1]);;
        spark.stop();
    }
}
