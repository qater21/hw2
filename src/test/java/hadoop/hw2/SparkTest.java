package hadoop.hw2;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.Test;

import com.holdenkarau.spark.testing.JavaDataFrameSuiteBase;

/**
 * 
 * @author Roman
 * @brief Test class for business logic. Dataframes are compared by spark-testing-base lib.
 */
public class SparkTest extends JavaDataFrameSuiteBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Test
	/**
	 * Testing aggregation with type avg
	 */
	public void avgTest() {
		/**
		 * Prepare dataframe
		 */
		List<String> inputData = new ArrayList<String>();
	    inputData.add("1, 1381363209, 10");
	    inputData.add("2, 1381363210, 20");
	    inputData.add("1, 1381363209, 30");
	    List<String> outputData = new ArrayList<String>();
	    outputData.add("1, 1381363209, 20");
	    outputData.add("2, 1381363210, 20");
	    SQLContext sqc = SQLContext.getOrCreate(jsc().sc());
		Dataset<Row> df = sqc.createDataset(inputData, Encoders.STRING()).toDF();
		df.show();
		df = df.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as value");
		
		Dataset<Row> expected = sqc.createDataset(outputData, Encoders.STRING()).toDF();
		expected = expected.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as agg_value" );
		expected = expected.selectExpr("metricid","timestamp","'avg' as type","cast(agg_value as double)");
		
		Dataset<Row> tested = Aggregator.process(df, "-avg");
		//JavaDataFrameSuiteBase sb = new JavaDataFrameSuiteBase();
		//expected = expected.withColumn("agg_value",expected.col("agg_value").cast(new DoubleType()));
		expected.show();
		tested.show();
		/**
		 * Compare tested and expected dataframes
		 */
		assertDataFrameEquals(expected, tested);
	}
	
	@Test
	/**
	 * Testing aggregation with type min
	 */
	public void minTest() {
		List<String> inputData = new ArrayList<String>();
	    inputData.add("1, 1381363209, 10");
	    inputData.add("2, 1381363210, 20");
	    inputData.add("1, 1381363209, 30");
	    List<String> outputData = new ArrayList<String>();
	    outputData.add("1, 1381363209, 10");
	    outputData.add("2, 1381363210, 20");
	    SQLContext sqc = SQLContext.getOrCreate(jsc().sc());
		Dataset<Row> df = sqc.createDataset(inputData, Encoders.STRING()).toDF();
		df.show();
		df = df.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as value");
		
		Dataset<Row> expected = sqc.createDataset(outputData, Encoders.STRING()).toDF();
		expected = expected.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as agg_value" );
		expected = expected.selectExpr("metricid","timestamp","'min' as type","agg_value");
		
		Dataset<Row> tested = Aggregator.process(df, "-min");
		//JavaDataFrameSuiteBase sb = new JavaDataFrameSuiteBase();
		//expected = expected.withColumn("agg_value",expected.col("agg_value").cast(new DoubleType()));
		expected.show();
		tested.show();
		assertDataFrameEquals(expected, tested);
	}
	
	@Test
	/**
	 * Testing aggregation with type sum
	 */
	public void sumTest() {
		List<String> inputData = new ArrayList<String>();
	    inputData.add("1, 1381363209, 10");
	    inputData.add("2, 1381363210, 20");
	    inputData.add("1, 1381363209, 30");
	    List<String> outputData = new ArrayList<String>();
	    outputData.add("1, 1381363209, 40");
	    outputData.add("2, 1381363210, 20");
	    SQLContext sqc = SQLContext.getOrCreate(jsc().sc());
		Dataset<Row> df = sqc.createDataset(inputData, Encoders.STRING()).toDF();
		df.show();
		df = df.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as value");
		
		Dataset<Row> expected = sqc.createDataset(outputData, Encoders.STRING()).toDF();
		expected = expected.selectExpr("split(value, ',')[0] as metricid", "split(value, ',')[1] as timestamp","split(value, ',')[2] as agg_value" );
		expected = expected.selectExpr("metricid","timestamp","'sum' as type","cast(agg_value as double)");
		
		Dataset<Row> tested = Aggregator.process(df, "-sum");
		//JavaDataFrameSuiteBase sb = new JavaDataFrameSuiteBase();
		//expected = expected.withColumn("agg_value",expected.col("agg_value").cast(new DoubleType()));
		expected.show();
		tested.show();
		assertDataFrameEquals(expected, tested);
	}
}
